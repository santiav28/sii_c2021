En este fichero se explicará brevemente las instrucciones para el correcto uso del mismo.

Instrucciones:

Teclas a utilizar:

--> 'w': jugador1 (izquierda) se mueve hacia arriba.
--> 's': jugador1 (izquierda) se mueve hacia abajo.
--> 'o': jugador2 (derecha) se mueve hacia arriba.
--> 'l': jugador2 (derecha) se mueve hacia abajo.
--> 'x': se detiene el movimiento de ambos jugadores hasta que pulsamos de nuevo alguna tecla para que se muevan.

