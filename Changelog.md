# Changelog 
Todos los cambios que se vayan introduciendo en el código quedarán reflejados en este documento.

## [1.4] - 2020-12-16
###
En esta cuerta versión del código se ha desarrollado un cliente y un servidor de forma que podríamos jugar desde distintos terminales. 
Ya no tendríamos un ejecutable "tenis", si no que ahora tenemos 2, "cliente" y "servidor".
Se sigue manteniendo el ejecutable "bot" que nos permite jugar en modo individual y "logger" que nos va indicando el tanteo.

## [1.3] - 2020-12-01
### Added
En esta tercera versión del código, se ha realizado, como cambios fundamentales, la creación de dos nuevos ejecutables, "logger" y "bot".
Con "logger" mostraremos por la terminal cuando un jugador ha anotado un punto y cuántos puntos lleva en ese momento. La partida se acabará cuando un jugador logre alcanzar la cifra de 4 puntos.
El ejecutable "bot", cuando lo activemos, controlará la raqueta del jugador 1, de esta forma tenemos una forma de juego que sería, jugar contra el ordenador.

## [1.2] - 2020-11-11
### Added
En esta segunda versión del código se ha añadido movimiento tanto a las raquetas como a la pelota.
Podremos controlar la posición de las raquetas en el eje "y" de forma que nos podamos colocar en la posición correcta para impactar con la pelota.
La pelota irá disminuyendo su tamaño a medida que avanza el tiempo que el punto está en juego hasta que uno de los 2 jugadores lo gana. Al iniciarse un nuevo punto vuelve a su tamaño original y comienza a decrecer otra vez.

Se ha añadido además un fichero README.txt donde se explican como controlar las raquetas.

## [1.1] - 2020-10-16
### Added
En esta primera versión simplemente se ha añadido en las cabeceras de los archivos.cpp el nombre del desarrollador.

