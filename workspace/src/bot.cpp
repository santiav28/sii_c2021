#include<iostream>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/mman.h>
#include<unistd.h>
#include<fcntl.h>
#include<iostream>

#include "DatosMemCompartida.h"
#include "Esfera.h"
#include "Raqueta.h"

int main(){

	//Se crea el fichero, el puntero a la memoria compartida y puntero a la proyeccion
	int fd;
	DatosMemCompartida* pMemComp;
	char* proyeccion;
	
	//Abrir fichero
	fd=open("/tmp/datosBot.txt",O_RDWR);
	if (fd < 0)
	{
	   printf("Error al abrir el bot\n");
	   return 1;
	};
	
	//Proyecto el fichero en el puntero
	proyeccion=(char*)mmap(NULL,sizeof(*(pMemComp)),PROT_WRITE|PROT_READ,MAP_SHARED,fd,0);

	//Cierro fichero
	close(fd);
	
	pMemComp= (DatosMemCompartida*)proyeccion;
	
	//Condicion de salida
	int salir=0;
	
	//Acciones de control de la raqueta(Explicado anteriormente)
	while(salir==0)
	{
		if(pMemComp->accion==5)
		{
			salir=1;
		}
		usleep(25000);
		std::cout<<"Hola soy el BOT\n";
		float posRaqueta;
		posRaqueta=((pMemComp->raqueta1.y2+pMemComp->raqueta1.y1)/2);
		if(posRaqueta<pMemComp->esfera.centro.y)
			pMemComp->accion=1;
		else if(posRaqueta>pMemComp->esfera.centro.y)
			pMemComp->accion=-1;
		else
			pMemComp->accion=0;
			
	}
	
	//Desmontamos proyeccion de memoria
	munmap(pMemComp,sizeof(*(pMemComp)));
}
