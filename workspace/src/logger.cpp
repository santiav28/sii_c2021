#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#define BUFSIZE 200

int main(int argc, char* argv[]){
	int fifo;
	fifo=mkfifo("/tmp/MiFifo", 0666);
	
	if(fifo==-1)
	{
		perror("mkfifo");
		return 1;
	}
	
	//abrimos en modo lectura
	int fd_out;
	fd_out=open("/tmp/MiFifo",O_RDONLY);
	
	if(fd_out==-1){
		perror("open");
		return 1;
	}
	
	int salir=0;
	int aux;// comprobamos si hay error en la lectura
	char buffer[BUFSIZE];
	while(salir==0)
	{
		
		aux=read(fd_out,buffer,BUFSIZE);
		printf("%s\n",buffer);
		if((buffer[0]=='-')||(aux==-1)||(aux==BUFSIZE))
		{
		printf("Tenis cerrado. Cerrando logger...\n");
		salir=1;
		}
	}
	close(fd_out);
	unlink("/tmp/MiFifo");
	return 0;
}
